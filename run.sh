#!/usr/bin/env bash

set -ex

./build.sh

cd build
pip3 install rangehttpserver
python3 -m RangeHTTPServer

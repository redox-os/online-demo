#!/usr/bin/env bash

set -ex

rm -rf build
mkdir -pv build

make -C v86
make -C v86 all

cp -v index.html build
cp -rv res build
cp -v v86/bios/seabios.bin build/res
cp -v v86/bios/vgabios.bin build/res
cp -v v86/build/libv86.js build/res
cp -v v86/build/v86.wasm build/res

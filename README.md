# online-demo

- Install Git LFS
- Install clang, nodejs, python3, and pip3
- Install Rust wasm target: `rustup target add wasm32-unknown-unknown`
- Clone the repository with --recursive, or update submodules after cloning
- Run ./run.sh
- Open http://localhost:8000/
